<?php
/**
 * @author      Marius Cucuruz
 * @copyright   Marius Cucuruz, 2018
 * 
 * @version     0.1
 * @package     MySQL Replication - Slave Status Check
 * 
 * This script checks the slave status on configured 
 * host(s) and sends an email alert respectively.
 * 
 * The possible failures could include:
 * 1) connection failure
 * 2) query failure (permissions, network, etc.)
 * 3) fetch failure (???)
 * 4) slave or io thread is not running
 * 5) Unknown master state (seconds_behind_master is null)
 * 6) seconds_behind_master has exceeded the configured threshold
 *
 */

/*******************************************
 * Email alert settings:
 ******************************************/
$mailTo         = "me@my-corp.com";
$mailFromAddr   = "alerts@my-corp.com";
$mailFromName   = "IT Alerts";
$mailSubject    = "REPLICATION ALERT";
$emailHeaders   = "From: $mailFromName <$mailFromAddr>\n" .
                    "Reply-To: $mailFromName <$mailFromAddr>\n" .
                    "MIME-Version: 1.0\n" .
                    "Content-Transfer-Encoding: 8bit\n" .
                    "X-Mailer: IT Alerts\n" .
                    "Content-Type: text/html;charset=UTF-8\r\n" .
                    #"Content-Type: text/plain; charset=utf-8\n" .
                    "";

/*******************************************
 * Define server(s) config:
 ******************************************/
$mysqlHosts     = array(
                    #['host.address',                    'username',     'password'],
                    array('mysql01.mydomain.local',      'rootuser',     'xxxxxxxx'),  # 
                    array('mysql06.mydomain.local',      'rootuser',     'xxxxxxxx'),  # 
                    #array('SERVERNAME',                 'some_user',    'xxxxxxxx'),  # another setup
                );
/*******************************************
 * SQL statement to run on slave:
 ******************************************/
$sql_stmt       = "SHOW SLAVE STATUS;";

/*******************************************
 * allowed Seconds_Behind_Master threshold:
 ******************************************/
$secThresholdBehindMaster = 1800;

/*******************************************
 * Turn reporting ON - better safe than sorry
 ******************************************/
error_reporting(E_ALL);

# define array to hold errors:
$errorsArray    = array();

# loop and check:
foreach ($mysqlHosts as $key => $slaveDetails)
{
    ## mysqli objective approach:
    try {
        # attempt connection:
        $dbConn1        = @new mysqli($slaveDetails[0], $slaveDetails[1], $slaveDetails[2]);

        # multi dimensional array for each server:
        $errorsArray[$slaveDetails[0]] = array();

        # if not connected alert:
        if ($dbConn1->connect_errno) 
        {
            $errorsArray[$slaveDetails[0]][] = sprintf("Connection to %s failed (user: %s): %s\n", $slaveDetails[0], $slaveDetails[1], $dbConn1->connect_error);
        }
        else {

            # check replication status:
            $checkStatus    = $dbConn1->query($sql_stmt);
            if (!$checkStatus)
            {
                $errorsArray[$slaveDetails[0]][] = sprintf ("Replication not running on %s or user %s is not allowed to check on %s.", $slaveDetails[0], $slaveDetails[1], $slaveDetails[0]);
            }
            else {

                while ($statusResult = $checkStatus->fetch_assoc() )
                {
                    #echo "<pre>MySQLi slave status {$slaveDetails[0]}:<br />". print_r($statusResult, true) ."</pre>";

                    # an error's been reported:
                    if ($statusResult['Last_Errno'] > 0)
                        $errorsArray[$slaveDetails[0]][]   = sprintf("Last error reported #%d: %s", $statusResult['Last_Errno'], $statusResult['Last_Error']);

                    # slave not running:
                    if ($statusResult['Slave_SQL_Running'] == 'No')
                        $errorsArray[$slaveDetails[0]][]   = sprintf("Slave is NOT running");

                    # cannot get master status:
                    if ($statusResult['Seconds_Behind_Master'] == null)
                        $errorsArray[$slaveDetails[0]][]   = sprintf("Slave unaware of master state (I/O State: %s).", $statusResult['Slave_IO_State']);

                    # slave is too far behind master
                    if ($statusResult['Seconds_Behind_Master'] > $secThresholdBehindMaster)
                        $errorsArray[$slaveDetails[0]][]   = sprintf("Slave running behind master by %d s.", $statusResult['Seconds_Behind_Master']);

                    # I/O not running:
                    if ($statusResult['Slave_IO_Running']  == 'No')
                        $errorsArray[$slaveDetails[0]][]   = sprintf("I/O Not running on slave (I/O State: ". $statusResult['Slave_IO_State'] .")");
                }
            }

            # free resources
            if ($dbConn1)
                $dbConn1->close();
        }
    }
    catch(Exception $e)
    {
        $errorsArray[$slaveDetails[0]][] =  sprintf("Connection to %s failed:<br />
                exception: %s<br />
                error #%d: %s<br />",
                $slaveDetails[0], $e->getMessage(), $dbConn1->connect_errno, $dbConn1->connect_error);
    }
    #die ("<pre>slave:". print_r($checkStatus->fetchAll(PDO::FETCH_ASSOC), true) ."</pre>");

    /**
     * @todo investigate the benefits of using PDO over MySQLi
     *
    ## PDO approach:
    try {
        $dbConn2        = new PDO("mysql:host={$slaveDetails[0]};", $slaveDetails[1], $slaveDetails[2]);
        $checkStatus    = $dbConn2->query($sql_stmt);

        if (!$checkStatus)
        {
            echo "<pre>[PDO] Replication is not running on {$slaveDetails[0]} (user:{$slaveDetails[1]}).</pre>";
            #throw new Exception("PDO Error [{$checkStatus->errno}] {$checkStatus->error}");
        }
        else {
            echo ("<pre>PDO slave status {$slaveDetails[0]}:". print_r($checkStatus->fetchAll(PDO::FETCH_ASSOC), true) ."</pre>");
        }

        # free resources
        if ($dbConn2)
            $dbConn2= NULL;
    }
    catch(PDOException $e)
    {
        printf("<pre>PDO Connection to %s failed:<br />
                exception: %s<br />
                error: %s<br /></pre>",
                $slaveDetails[0], $e->getMessage(), $dbConn2->connect_error);
        #echo "PDO Connection to {$slaveDetails[0]} failed: " . $e->getMessage();
    }

    #die ("<pre>slave:". print_r($checkStatus, true) ."</pre>");
    echo "<hr />";
    */
}

foreach ($errorsArray as $server => $errorsArr)
{
    /*******************************************
     * send individual email alerts per server:
     ******************************************/
    if (count($errorsArr) > 0)
    {

        ob_start(); // start output buffer

        echo "<p>Encountered errors on $server</p>";
        echo "<ul>";
        foreach ($errorsArr as $error)
        {
            echo "<li>$error</li>";
        }
        echo "</ul>";

        $hmtlMessage = ob_get_contents(); // fetch the contents of the buffer
        ob_end_clean(); // cleanup the buffer

        try {
            # send email alert:
            mail($mailTo, "$mailSubject - $server", $hmtlMessage, $emailHeaders);
            printf ("<p>Alerted %s (%s - %s)</p>", $mailTo, $mailSubject, $server);
        }
        catch(Exception $e)
        {
            printf("Sending email failed with exception: %s", $e->getMessage());
        }
    }
}

#die ("<pre>any errors?". print_r($errorsArray, true) ."</pre>");
